// module requires
const { codeBlock } = require("discord.js");
const { capitalizeString, dynamicSort, capitalizeAll, splitMessage } = require("../../functionFiles/helperFunctions.js");

exports.run = async (client, message, args) => {
	// If no specific command is called, show all filtered commands.
	if (args.length == 0) {
		// variables for different things used
		let longestName = 0;
		let categories = {};
		let userAllowedCmds = [];

		// check what perms the user has and add to userAllowedCmds
		if(message.guild) {
			if (message.member.permissions.has('SendMessages')) userAllowedCmds.push("User");
			if (message.member.permissions.has('ViewAuditLog')) userAllowedCmds.push("modAssist", "User");
			if (message.member.permissions.has('Administrator')) userAllowedCmds.push("serverAdmin", "modAssist", "User");
		}

		// loop through the commands, checking that it's a guild only one and check if the user perms are enough to use the command (or is bot admin)
		client.commands.forEach(command => {
			if (message.guild && command.config.guildOnly &&
				(userAllowedCmds.includes(command.config.permLevel) ||
				(client.hardcoded.botAdmin.includes(`${message.author.id}`) && command.config.permLevel !== "User"))) {
				// check for the longest name
				longestName = Math.max(longestName, command.config.name.length);

				// add the command to the categories object, with the key, if that exists just add the command to the array under it
				if (!Object.keys(categories).includes(command.config.category)) {
					categories[command.config.category] = [];
				}

				// add the command to the category
				categories[command.config.category].push(command.config);
			} else { // for DMs
				if (client.hardcoded.DMModules.includes(command.config.category) || command.config.name === "help") {
          if(command.config.guildOnly === false) {
            // check for the longest name
            longestName = Math.max(longestName, command.config.name.length);

            // add the command to the categories object, with the key, if that exists just add the command to the array under it
            if (!Object.keys(categories).includes(command.config.category)) {
              categories[command.config.category] = [];
            }

            // add the command to the category
            categories[command.config.category].push(command.config);
          }
				}
			}
		});

		// create the "header" for the message
		let output = (`== Command List ==\n` +
			`[Use ${client.prefix}helpme <command> for details]\n\n`);

		// create the blocks for each category and add that to output
		for (catKey of Object.keys(categories).sort()) {
			if(!message.guild){
				{ // for DMs
				   // add the category header to the output
				   output += `\u200b\n== ${capitalizeAll(catKey)} ==\n`;

				   // sort the list by name
				   categories[catKey].sort(dynamicSort("name"));

				   // loop through all the commands under each category, add padding for all to be same length
				   for (cmd of categories[catKey]) {
					   output += `${cmd.name.padEnd(longestName, " ")} :: ${cmd.description}\n`
				   }
			   }
		   } else if (client.hardcoded.botAdmin.includes(message.author.id) ? {} : client.cache[message.guild.id].enabledModules.includes(catKey)){ // for guilds
				// add the category header to the output
				output += `\u200b\n== ${capitalizeAll(catKey)} ==\n`;

				// sort the list by name
				categories[catKey].sort(dynamicSort("name"));

				// loop through all the commands under each category, add padding for all to be same length
				for (cmd of categories[catKey]) {
					output += `${cmd.name.padEnd(longestName, " ")} :: ${cmd.description}\n`
				}
			}
		}
		let splitOutput = splitMessage(output, {maxLength: 1900, char: "\u200b"});
		for (let chunk of splitOutput) {
			await message.channel.send(codeBlock("asciidoc", chunk.trim()));
		}
	} else {
		// Show individual command's help.
		let command = args;
		if (client.commands.has(command)) {
			command = client.commands.get(command);
		} else if (client.aliases.has(command)){
			command = client.commands.get(client.aliases.get(command));
		} else if (Object.keys(client.shortcuts).includes(command)) {
			command = client.commands.get(client.shortcuts[command])
		}
		if(command){
			// if (level < client.levelCache[command.config.permLevel])
			// 	return;
			message.channel.send(codeBlock('asciidoc',
				`== ${capitalizeString(command.config.name)} == \n` +
				`${command.config.description}\n` +
				`usage   :: ${command.config.name} ${command.config.usage.join(`\n${" ".repeat(11) + command.config.name} `)}\n` +
				`aliases :: ${command.config.aliases.join(", ") || "no aliases"}`));
		}
	}
};

exports.config = {
	guildOnly: false,
	aliases: ["h", "halp","helpme"],
	permLevel: "User",
	name: "help",
	description: "Displays all available commands for a user's permission level.",
	usage: ["(<command>)"]
};
